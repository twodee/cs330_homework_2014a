import Funfun
import System.Exit

assertEquals message expected actual = do
  if expected == actual then
    return ()
  else do
    putStrLn message
    putStr $ "  Expected: "
    print expected
    putStr $ "    Actual: "
    print actual
    System.Exit.exitFailure
    return ()

assertTrue message actual = assertEquals message True actual
assertFalse message actual = assertEquals message False actual

main = do
  assertEquals "everyOther doesn't return the correct list." ['a','c','e'] (everyOther ['a'..'f'])
  assertEquals "everyOther doesn't return the correct list." ['a','c','e'] (everyOther ['a'..'e'])
  assertEquals "everyOther doesn't return the correct list." [] (everyOther [] :: [Int])
  assertEquals "everyOther doesn't return the correct list." ["foo"] (everyOther ["foo"])

  assertEquals "fsts doesn't return the correct list of first fields." ['a', 'b', 'c'] (fsts [('a', 1), ('b', 2), ('c', 3)])
  assertEquals "fsts doesn't return the correct list of first fields." [10, 11, 100] (fsts [(10, 1), (11, 2), (100, 3)])
  assertEquals "fsts doesn't return the correct list of first fields." [] (fsts [] :: [Int])
  assertEquals "snds doesn't return the correct list of second fields." [1..3] (snds [('a', 1), ('b', 2), ('c', 3)])
  assertEquals "snds doesn't return the correct list of second fields." [1..3] (snds [(10, 1), (11, 2), (100, 3)])
  assertEquals "snds doesn't return the correct list of second fields." [] (snds [] :: [Int])

  assertEquals "bounds1Accum base case returned wrong pair." (5, 100) (bounds1Accum [] 5 100)
  assertEquals "bounds1Accum base case returned wrong pair." (100, 5) (bounds1Accum [] 100 5)
  assertEquals "bounds1Accum general case returned wrong pair." (1, 1) (bounds1Accum [1, 1, 1, 1] 1 1)
  assertEquals "bounds1Accum general case returned wrong pair." (1, 1) (bounds1Accum [1, 1, 1, 1] 10 (-10))
  assertEquals "bounds1Accum general case returned wrong pair." (0, 10) (bounds1Accum [1, 10, 0, 1] 5 5)

  assertEquals "bounds1 base case failed to return Nothing." Nothing (bounds1 [] :: Maybe (Int, Int))
  assertEquals "bounds1 general case return wrong min-max pair." (Just (0, 0)) (bounds1 [0])
  assertEquals "bounds1 general case return wrong min-max pair." (Just (0, 10)) (bounds1 [0, 10])
  assertEquals "bounds1 general case return wrong min-max pair." (Just ('a', 'b')) (bounds1 ['a', 'b', 'b', 'a'])

  assertEquals "bounds2 base case returned wrong min-max pair." (Nothing, Nothing) (bounds2 [] :: (Maybe (Int, Int), Maybe (Int, Int)))
  assertEquals "bounds2 general case returned wrong min-max pair." (Just (1, 10), Just ('a', 'z')) (bounds2 [(2, 'a'), (1, 'm'), (10, 'z')])
  assertEquals "bounds2 general case returned wrong min-max pair." (Just (1, 1), Just ('z', 'z')) (bounds2 [(1, 'z')])

  assertEquals "filterMask base case returned wrong list." [] $ filtermask (\m d -> m) [] ([] :: [Int])
  assertEquals "filterMask general case returned wrong list." ['a', 'c'] $ filtermask (\m d -> m) [True, False, True, False] ['a', 'b', 'c', 'd']
  assertEquals "filterMask general case returned wrong list." [1..5] $ filtermask (\m d -> d < m) (replicate 10 6) [1..10]

  assertEquals "filterTrues returned wrong list." ['a', 'c'] $ filterTrues [True, False, True, False] ['a', 'b', 'c', 'd']
  assertEquals "filterTrues returned wrong list." ["my", "is"] $ filterTrues [False, False, True, False, True] ["dog", "brown", "my", "smith", "is"]

  assertEquals "sumTrues returned wrong sum." 9 $ sumTrues [True, False, True, False, True] [1..5]
  assertEquals "sumTrues returned wrong sum." 0 $ sumTrues [] []

  assertEquals "filterDiffs returned wrong list." ["favorite", "color"] $ filterDiffs ["my", "favourite", "colour", "is"] ["my", "favorite", "color", "is"]
  assertEquals "filterDiffs returned wrong list." [] $ filterDiffs ["my", "favourite", "colour", "is"] ["my", "favourite", "colour", "is"]

  assertEquals "joinDiffs returned wrong string." "favorite\ncolor\n" $ joinDiffs ["my", "favourite", "colour", "is"] ["my", "favorite", "color", "is"]
  assertEquals "joinDiffs returned wrong string." "" $ joinDiffs ["my", "favourite", "colour", "is"] ["my", "favourite", "colour", "is"]

  assertEquals "Row's minBound is simply not okay." A (minBound :: Row)
  assertEquals "Row's maxBound is simply not okay." J (maxBound :: Row)
  assertEquals "Column's minBound is simply not okay." One (minBound :: Column)
  assertEquals "Column's maxBound is simply not okay." Ten (maxBound :: Column)

  let positions = [Address A One,Address A Two,Address A Three,Address A Four,Address A Five,Address A Six,Address A Seven,Address A Eight,Address A Nine,Address A Ten,Address B One,Address B Two,Address B Three,Address B Four,Address B Five,Address B Six,Address B Seven,Address B Eight,Address B Nine,Address B Ten,Address C One,Address C Two,Address C Three,Address C Four,Address C Five,Address C Six,Address C Seven,Address C Eight,Address C Nine,Address C Ten,Address D One,Address D Two,Address D Three,Address D Four,Address D Five,Address D Six,Address D Seven,Address D Eight,Address D Nine,Address D Ten,Address E One,Address E Two,Address E Three,Address E Four,Address E Five,Address E Six,Address E Seven,Address E Eight,Address E Nine,Address E Ten,Address F One,Address F Two,Address F Three,Address F Four,Address F Five,Address F Six,Address F Seven,Address F Eight,Address F Nine,Address F Ten,Address G One,Address G Two,Address G Three,Address G Four,Address G Five,Address G Six,Address G Seven,Address G Eight,Address G Nine,Address G Ten,Address H One,Address H Two,Address H Three,Address H Four,Address H Five,Address H Six,Address H Seven,Address H Eight,Address H Nine,Address H Ten,Address I One,Address I Two,Address I Three,Address I Four,Address I Five,Address I Six,Address I Seven,Address I Eight,Address I Nine,Address I Ten,Address J One,Address J Two,Address J Three,Address J Four,Address J Five,Address J Six,Address J Seven,Address J Eight,Address J Nine,Address J Ten]
  assertEquals "allAddressesA is not the correct list of addresses." positions allAddressesA
  assertEquals "allAddressesB is not the correct list of addresses." positions allAddressesB

  assertEquals "targetShip fails on the empty ship." (Ship []) (targetShip (Ship []) (Address A One))
  assertEquals "targetShip incorrectly targets a ship." (Ship [Cell (Address A Two) False]) (targetShip (Ship [Cell (Address A Two) False]) (Address A One))
  assertEquals "targetShip incorrectly targets a ship." (Ship [Cell (Address A Two) True]) (targetShip (Ship [Cell (Address A Two) False]) (Address A Two))
  assertEquals "targetShip incorrectly targets a ship." (Ship [Cell (Address A Two) True, Cell (Address B Two) True]) (targetShip (Ship [Cell (Address A Two) True, Cell (Address B Two) False]) (Address B Two))
  assertEquals "targetShip incorrectly targets a ship." (Ship [Cell (Address A Two) True, Cell (Address B Two) False]) (targetShip (Ship [Cell (Address A Two) True, Cell (Address B Two) False]) (Address A Two))

  assertTrue "isSunk is incorrect for the empty ship, which is always sunk." (isSunk $ Ship [])
  assertTrue "isSunk incorrectly reports that a sunk ship is not sunk." (isSunk (Ship [Cell (Address A Two) True]))
  assertTrue "isSunk incorrectly reports that a sunk ship is not sunk." (isSunk (Ship [Cell (Address A Two) True, Cell (Address B Two) True]))
  assertTrue "isSunk incorrectly reports that a sunk ship is not sunk." (isSunk (Ship [Cell (Address A Two) True, Cell (Address B Two) True, Cell (Address C Two) True]))
  assertFalse "isSunk incorrectly reports than an unsunk ship is sunk." (isSunk (Ship [Cell (Address A Two) True, Cell (Address B Two) True, Cell (Address C Two) False]))

  assertEquals "targetShips fails on the empty list of ships." (Ships []) (targetShips (Ships []) (Address A One))
  assertEquals "targetShips incorrectly targets a ship." (Ships [Ship [Cell (Address A Two) False]]) (targetShips (Ships [Ship [Cell (Address A Two) False]]) (Address A One))
  assertEquals "targetShips incorrectly targets a ship." (Ships [Ship [Cell (Address A Two) True]]) (targetShips (Ships [Ship [Cell (Address A Two) False]]) (Address A Two))
  assertEquals "targetShips incorrectly targets a ship." (Ships [Ship [Cell (Address A Two) True, Cell (Address B Two) True]]) (targetShips (Ships [Ship [Cell (Address A Two) True, Cell (Address B Two) False]]) (Address B Two))
  assertEquals "targetShips incorrectly targets a ship." (Ships [Ship [Cell (Address A Two) True, Cell (Address B Two) False]]) (targetShips (Ships [Ship [Cell (Address A Two) True, Cell (Address B Two) False]]) (Address A Two))

  assertEquals "targetShips incorrectly targets a ship." (Ships [Ship [Cell (Address J Ten) False], Ship [Cell (Address A Two) False]]) (targetShips (Ships [Ship [Cell (Address J Ten) False], Ship [Cell (Address A Two) False]]) (Address A One))
  assertEquals "targetShips incorrectly targets a ship." (Ships [Ship [Cell (Address J Ten) False], Ship [Cell (Address A Two) True]]) (targetShips (Ships [Ship [Cell (Address J Ten) False], Ship [Cell (Address A Two) False]]) (Address A Two))
  assertEquals "targetShips incorrectly targets a ship." (Ships [Ship [Cell (Address J Ten) False], Ship [Cell (Address A Two) True, Cell (Address B Two) True]]) (targetShips (Ships [Ship [Cell (Address J Ten) False], Ship [Cell (Address A Two) True, Cell (Address B Two) False]]) (Address B Two))
  assertEquals "targetShips incorrectly targets a ship." (Ships [Ship [Cell (Address J Ten) False], Ship [Cell (Address A Two) True, Cell (Address B Two) False]]) (targetShips (Ships [Ship [Cell (Address J Ten) False], Ship [Cell (Address A Two) True, Cell (Address B Two) False]]) (Address A Two))

  assertEquals "targetShips incorrectly targets a ship." (Ships [Ship [Cell (Address J Ten) False, Cell (Address A One) True], Ship [Cell (Address A Two) False]]) (targetShips (Ships [Ship [Cell (Address J Ten) False, Cell (Address A One) False], Ship [Cell (Address A Two) False]]) (Address A One))
  assertEquals "targetShips incorrectly targets a ship." (Ships [Ship [Cell (Address J Ten) False, Cell (Address A Two) True], Ship [Cell (Address A Two) True]]) (targetShips (Ships [Ship [Cell (Address J Ten) False, Cell (Address A Two) False], Ship [Cell (Address A Two) False]]) (Address A Two))
  assertEquals "targetShips incorrectly targets a ship." (Ships [Ship [Cell (Address J Ten) False, Cell (Address A One) False], Ship [Cell (Address A Two) True, Cell (Address B Two) True]]) (targetShips (Ships [Ship [Cell (Address J Ten) False, Cell (Address A One) False], Ship [Cell (Address A Two) True, Cell (Address B Two) False]]) (Address B Two))
  assertEquals "targetShips incorrectly targets a ship." (Ships [Ship [Cell (Address J Ten) False, Cell (Address A One) False], Ship [Cell (Address A Two) True, Cell (Address B Two) False]]) (targetShips (Ships [Ship [Cell (Address J Ten) False, Cell (Address A One) False], Ship [Cell (Address A Two) True, Cell (Address B Two) False]]) (Address A Two))
