import Iso
import System.Exit

assertEqualsHelper op message expected actual = do
  if op expected actual then
    return ()
  else do
    putStrLn message
    putStr $ "  Expected: "
    print expected
    putStr $ "    Actual: "
    print actual
    System.Exit.exitFailure
    return ()

assertEquals message expected actual = assertEqualsHelper (==) message expected actual
assertTrue message actual = assertEquals message True actual
assertFalse message actual = assertEquals message False actual

xyCompare :: XY -> XY -> Bool
xyCompare (expectedX, expectedY) (actualX, actualY) = abs(expectedX - actualX) < 0.001 && abs(expectedY - actualY) < 0.001

xysCompare :: [XY] -> [XY] -> Bool
xysCompare expecteds actuals = (length expecteds == length actuals) && (and $ map (\(e, a) -> xyCompare e a) $ zip expecteds actuals)

translateTest :: XY -> XY -> XY -> IO ()
translateTest original delta expected = assertEqualsHelper xyCompare ("translate " ++ (show original) ++ " by " ++ (show delta) ++ " yields incorrect XY") expected (translate original delta)

scaleTest :: XY -> XY -> XY -> IO ()
scaleTest original delta expected = assertEqualsHelper xyCompare ("scale " ++ (show original) ++ " by " ++ (show delta) ++ " yields incorrect XY") expected (scale original delta)

rotateTest :: XY -> Double -> XY -> IO ()
rotateTest original delta expected = assertEqualsHelper xyCompare ("rotate " ++ (show original) ++ " by " ++ (show delta) ++ " yields incorrect XY") expected (rotate original delta)

translateTest' :: XY -> XY -> XY -> IO ()
translateTest' original (dx, dy) expected = assertEqualsHelper xyCompare ("xform " ++ (show original) ++ " by " ++ (show (Translate dx dy)) ++ " yields incorrect XY") expected (xform (Translate dx dy) original)

scaleTest' :: XY -> XY -> XY -> IO ()
scaleTest' original (sx, sy) expected = assertEqualsHelper xyCompare ("xform " ++ (show original) ++ " by " ++ (show (Scale sx sy)) ++ " yields incorrect XY") expected (xform (Scale sx sy) original)

rotateTest' :: XY -> Double -> XY -> IO ()
rotateTest' original delta expected = assertEqualsHelper xyCompare ("xform " ++ (show original) ++ " by " ++ (show (Rotate delta)) ++ " yields incorrect XY") expected (xform (Rotate delta) original)

main = do
  assertEquals "twoByTwo yields incorrect result for even-lengthed list" [(1,2), (3,4), (5,6)] (twoByTwo [1..6])
  assertEquals "twoByTwo yields incorrect result for odd-lengthed list" [(1,2), (3,4), (5,6)] (twoByTwo [1..7])
  assertEquals "twoByTwo yields incorrect result for even-lengthed list" [(1,2), (3,4), (5,6), (7,8)] (twoByTwo [1..8])
  assertEquals "twoByTwo yields incorrect result for even-lengthed list" [('a', 'b'), ('c', 'd'), ('e', 'f')] (twoByTwo "abcdef")
  assertEquals "twoByTwo yields incorrect result for odd-lengthed list" [('a', 'b'), ('c', 'd'), ('e', 'f')] (twoByTwo "abcdefg")

  assertEquals "mode yields incorrect value for string" 'e' (mode "aaksdjfweirewewrew re ee rer er ere arerkse")
  assertEquals "mode yields incorrect value for list of numbers" 1 (mode (1 : [1..1000]))
  assertEquals "mode yields incorrect value for list of strings" "the" (mode ["the", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog"])

  assertEquals "indices yields incorrect list for \" \" in \"the quick brown fox\"" [3, 9, 15] (indices " " "the quick brown fox")
  assertEquals "indices yields incorrect list for \"danger\" in \"the quick brown fox\"" [] (indices "danger" "the quick brown fox")
  assertEquals "indices yields incorrect list for [1,2] in [1,2,3,4,1,2,1,4,1,3,2,7]" [0,4] (indices [1,2] [1,2,3,4,1,2,1,4,1,3,2,7])
  assertEquals "indices yields incorrect list for [1,2,7,9] in []" [] (indices [1,2,7,9] [])
  assertEquals "indices yields incorrect list for [1,2,7,9] in [1,2,7]" [] (indices [1,2,7,9] [1,2,7])

  assertEquals "sumOfSquares yields incorrect result for list of ones" 3 (sumOfSquares [1, 1, 1])
  assertEquals "sumOfSquares yields incorrect result for list [1, 2, 0]" 5 (sumOfSquares [1, 2, 0])
  assertEquals "sumOfSquares yields incorrect result for list [1, 2, 5, 100, -3]" (1+4+25+10000+9) (sumOfSquares [1, 2, 5, 100, -3])

  assertEquals "squareOfSum yields incorrect result for [1..3]" 36 (squareOfSum [1..3])
  assertEquals "squareOfSum yields incorrect result for [0,0,0,0]" 0 (squareOfSum [0, 0, 0, 0])
  assertEquals "squareOfSum yields incorrect result for [10,11,30,-12]" 1521 (squareOfSum [10,11,30,-12])

  assertEquals "nroutes yields incorrect result for 3x3" 20 (nroutes 3 3)
  assertEquals "nroutes yields incorrect result for 4x4" 70 (nroutes 4 4)
  assertEquals "nroutes yields incorrect result for 5x5" 252 (nroutes 5 5)
  assertEquals "nroutes yields incorrect result for 6x6" 924 (nroutes 6 6)
  assertEquals "nroutes yields incorrect result for 7x7" 3432 (nroutes 7 7)
  assertEquals "nroutes yields incorrect result for 20x20" 137846528820 (nroutes 20 20)
  assertEquals "nroutes yields incorrect result for 4x1" 5 (nroutes 4 1)
  assertEquals "nroutes yields incorrect result for 12x5" 6188 (nroutes 12 5)
  assertEquals "nroutes yields incorrect result for 3x10" 286 (nroutes 3 10)

  translateTest (5, 4) (1, 2) (6, 6)
  translateTest (100, -1000) (0, 0) (100, -1000)
  translateTest (4, 7) (100, 200) (104, 207)
  translateTest (4.3, 7.9) (0.6, 0.01) (4.9, 7.91)

  scaleTest (0, 0) (1, 1) (0, 0)
  scaleTest (100, 50) (2, 2) (200, 100)
  scaleTest (50, 30) (0.1, 0.2) (5, 6)
  scaleTest (7.1, 6.2) (-1, -2) (-7.1, -12.4)

  rotateTest (5, 1) (pi / 2) (-1, 5)
  rotateTest (5, 1) (pi / (-2)) (1, -5)
  rotateTest (9, -9) (pi / 2) (9, 9)
  rotateTest (0, 0) (pi / 6) (0, 0)
  rotateTest (100, 3) (2 * pi) (100, 3)
  rotateTest (100, 3) (-pi) (-100, -3)

  translateTest' (5, 4) (1, 2) (6, 6)
  translateTest' (100, -1000) (0, 0) (100, -1000)
  translateTest' (4, 7) (100, 200) (104, 207)
  translateTest' (4.3, 7.9) (0.6, 0.01) (4.9, 7.91)

  scaleTest' (0, 0) (1, 1) (0, 0)
  scaleTest' (100, 50) (2, 2) (200, 100)
  scaleTest' (50, 30) (0.1, 0.2) (5, 6)
  scaleTest' (7.1, 6.2) (-1, -2) (-7.1, -12.4)

  rotateTest' (5, 1) (pi / 2) (-1, 5)
  rotateTest' (5, 1) (pi / (-2)) (1, -5)
  rotateTest' (9, -9) (pi / 2) (9, 9)
  rotateTest' (0, 0) (pi / 6) (0, 0)
  rotateTest' (100, 3) (2 * pi) (100, 3)
  rotateTest' (100, 3) (-pi) (-100, -3)

  assertEquals "Unexpected inverse transform" (Translate (-5) (-10)) (invertTransform (Translate 5 10))
  assertEquals "Unexpected inverse transform" (Translate (55) (-1)) (invertTransform (Translate (-55) 1))
  assertEquals "Unexpected inverse transform" (Translate (0) (-17)) (invertTransform (Translate (0) 17))

  assertEquals "Unexpected inverse transform" (Scale (10) (5)) (invertTransform (Scale (0.1) (0.2)))
  assertEquals "Unexpected inverse transform" (Scale (4) (-0.02)) (invertTransform (Scale (0.25) (-50)))

  assertEquals "Unexpected inverse transform" (Rotate (pi)) (invertTransform (Rotate (-pi)))
  assertEquals "Unexpected inverse transform" (Rotate (45)) (invertTransform (Rotate (-45)))
  assertEquals "Unexpected inverse transform" (Rotate (0)) (invertTransform (Rotate (0)))
  assertEquals "Unexpected inverse transform" (Rotate (-1.6)) (invertTransform (Rotate (1.6)))

  assertEqualsHelper xyCompare "xxxform with short transforms list yields incorrect result" (280.0755367394752,-84.88759412793783) (xxxform [Rotate 5, Translate 34 (-10), Scale 5 6.1] (10, 20))
  assertEqualsHelper xyCompare "xxxform with medium transforms list yields incorrect result" (-144.82734575159543,285.6404523797286) (xxxform [Rotate (-pi), Scale 99.9 4.7, Scale 1 2, Rotate ((-pi) / 4), Translate 12 11.5, Translate 3 (-4)] (3.1, -8.9))
  assertEqualsHelper xyCompare "xxxform with long transforms list yields incorrect result" (189.60759475726962,-246.45638973583021) (xxxform [Scale (-1.4) 1.1, Rotate 0.5, Rotate (-3), Translate 5 6, Translate (-3) (-10), Translate 87 (-90), Rotate 12, Scale 1.1 1.1] (100, 98))

  assertEquals "Unexpected bounding box for short list of XYs" (-41.69298932356919,-23.100194075755077,-18.30817870553849,45.92120147022631) (bbox [(-25.367832399559454,8.095931686531635),(-18.30817870553849,45.92120147022631),(-27.84115089246707,-22.881868658795945),(-31.880053329712588,-23.100194075755077),(-41.69298932356919,13.545606712964883)])
  assertEquals "Unexpected bounding box for medium list of XYs" (-45.10966888139281,-32.483403463431046,47.3203827792136,40.63822473845464) (bbox [(-9.851632390540878,17.68371068860634),(25.688067745934305,14.776837504963964),(38.836059420620785,25.989385087609023),(-45.10966888139281,-10.996746951333101),(-30.088795570357906,-8.644809312181565),(-32.521559651168204,40.63822473845464),(-30.13378537453104,-12.641652867979673),(4.008635691710374,-32.483403463431046),(12.647837794735182,0.6690484236028595),(47.3203827792136,22.79544323849001)])
  assertEquals "Unexpected bounding box for long list of XYs" (-30.790311807872072,-44.76666754714822,49.800386116151856,49.85380183563596) (bbox [(3.3489830425825104,-9.644682150907002),(-15.109421987428405,49.85380183563596),(19.77727558251685,0.35633502525374894),(43.13237976596659,-9.209717519803917),(-11.52277113415957,24.52143630448819),(-2.247790146703899,17.88895280801904),(1.096476045156372,-44.76666754714822),(49.800386116151856,-2.856421540971411),(48.11787223272039,-10.942452508698288),(-15.938376509170674,1.3462389484104733),(47.8585578374165,18.152810203979868),(44.15178639253226,27.093865078369475),(31.850640119601337,-25.606527399659505),(-30.790311807872072,16.52872654634014),(21.36483115172119,38.11271062072531),(16.915863494338595,-15.98815370062482),(38.7031456569011,-13.69330410983688),(-2.179127347635898,7.405847810049416),(5.560774468695499,-11.19316855698844),(17.469881325106982,5.3348863472993955)])

  assertEquals "Unexpected centroid for short list of XYs" (-6.517405172608189,-6.563340499334078) (centroid [(18.923853820112896,-13.430424566375784),(-48.5431134611623,-45.616015618167474),(-7.1838072105501,-24.968102736377872),(-41.72720949897287,-43.33614922964269),(35.50830311594592,32.48933461949932)])
  assertEquals "Unexpected centroid for medium list of XYs" (11.788150474844613,-1.6824797865919585) (centroid [(-12.989907338620299,8.71676347416063),(30.121186553222884,31.873082380058875),(33.04189490203214,-24.16523302639837),(24.78443364791454,31.526071604637224),(19.637966505677056,-41.620162421188596),(-20.458062627548912,44.25965739608043),(30.86565608595255,-47.624616969264345),(13.70615666767825,32.97514357854267),(44.03436357723814,25.886818378519038),(35.75364596948614,-38.23029860771294)])
  assertEquals "Unexpected centroid for long list of XYs" (-0.41568266371743334,1.1565522179254408) (centroid [(31.66571715911087,18.695390552350574),(-47.74819518146869,-28.52198346534468),(-3.1801420734453387,-16.1575792868636),(48.967072985351464,8.96493131499895),(2.5384460586266897,-7.20746447749606),(40.92005335817433,28.72997710300035),(1.1300297191583013,15.339691760680893),(-35.70518695808863,18.899674856168474),(22.102533268976458,42.10292903470085),(-5.9947377526542525,5.890048251018001),(-49.79843831278633,22.45156182927792),(-38.914741733257884,-5.546879643368342),(-30.601951064973143,-39.13446094519584),(28.36458331865434,42.129393350971625),(-0.7611405405008824,-39.245236125140735),(-16.879342146072872,11.065439535772349),(-37.75892223575666,-23.03350667476026),(33.98577656318638,10.936678015998616),(6.328204461208628,-39.816288915120744),(-10.475121485444582,-25.54534171118369)])

  assertEqualsHelper xysCompare "Unexpected center list for short list of XYs" [(-2.0460916585818403,-39.74712765516959),(28.470868629690283,32.76760555185993),(-28.470868629690283,31.45840807720809),(1.0711064665639398,23.578257665226985),(-5.681735515319108,39.74712765516959)] (center [(-21.3117867534109,-42.37341611146718),(9.205173534861224,30.141317095562343),(-47.73656372451934,28.832119620910504),(-18.19458862826512,20.9519692089294),(-24.947430610148167,37.120839198872005)])
  assertEqualsHelper xysCompare "Unexpected center list for medium list of XYs" [(18.878353640733838,27.100507113969154),(-41.30513612910312,-42.05485229324934),(-49.036627481931845,3.237357730731059),(2.6478025575228408,33.05902528780145),(-15.256787166831156,19.39584714166659),(18.4084255969553,-5.399969058976001),(-13.091407276098376,42.05485229324934),(19.296928448154876,16.262267004092617),(49.036627481931845,34.22893406694051),(41.502922463144756,-25.187565951811823)] (center [(19.21418945769875,34.56903459594713),(-40.9693003121382,-34.58632481127136),(-48.700791664966935,10.705885212709035),(2.9836383744877537,40.52755276977942),(-14.920951349866243,26.864374623644565),(18.744261413920213,2.0685584230019742),(-12.755571459133463,49.52337977522731),(19.63276426511979,23.730794486070593),(49.37246329889676,41.69746154891848),(41.83875828010967,-17.719038469833848)])
  assertEqualsHelper xysCompare "Unexpected center list for long list of XYs" [(-35.45882223545808,-35.7050612059514),(42.08760805260523,6.578833519010821),(3.5808791291687854,39.13300764467553),(-41.763663407111984,40.24566027401421),(27.677440024375002,-8.141266659054367),(8.127587144069409,-15.008975855677662),(-46.32863079027845,24.23096112956894),(-10.199927898448394,-2.1288686590109407),(-39.462527901648606,43.917630332238915),(-26.05791386600144,-43.25761206644349),(-24.404272968522648,-7.55892553880598),(32.13150504337685,15.884154614301629),(44.01757169310473,35.55502111177303),(-17.160928967239066,-34.978650062320135),(-38.50403285296126,-45.370011824870915),(46.32863079027845,22.11783584666678),(-37.34905655023591,45.370011824870915),(12.727492540372019,28.321425013867525),(-38.099972303802026,-24.249330236238904),(6.328911164572233,-34.42224620070513)] (center [(-36.334866965435886,-38.68342132326311),(41.21156332262743,3.6004734016991122),(2.704834399190979,36.15464752736382),(-42.63970813708979,37.267300156702504),(26.801395294397196,-11.119626776366076),(7.2515424140916025,-17.98733597298937),(-47.20467552025626,21.25260101225723),(-11.0759726284262,-5.10722877632265),(-40.33857263162641,40.939270214927205),(-26.933958595979245,-46.2359721837552),(-25.280317698500454,-10.537285656117689),(31.25546031339904,12.90579449698992),(43.14152696312692,32.57666099446132),(-18.036973697216872,-37.957010179631844),(-39.380077582939066,-48.348371942182624),(45.452586060300646,19.13947572935507),(-38.225101280213714,42.391651707559205),(11.851447810394212,25.343064896555816),(-38.97601703377983,-27.227690353550614),(5.452866434594426,-37.40060631801684)])
