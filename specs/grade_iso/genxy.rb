#!/usr/bin/env ruby

g = Random.new
n = ARGV[0].to_i
xys = (1..n).map do |i|
  "(#{g.rand * 100 - 50},#{g.rand * 100 - 50})"
end

puts "[#{xys.join(',')}]"
