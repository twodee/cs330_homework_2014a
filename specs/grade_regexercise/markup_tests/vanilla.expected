<h1>Regexps</h1>

A Regexp represents a regular expression: a pattern that describes a String. If a String contains the pattern described by a given regular expression, it is said to match. Therefore, one use of regular expressions is validation: testing whether a String matches a pattern.

<h2>Literals</h2>

The literal is of the form /pattern/options: a pattern delimited by solidi, followed by zero or more single-character option specifiers. If the pattern is to contain either of these delimiters literally, they must be escaped with a reverse solidus. The construct %r{pattern}options also constructs a Regexp, but in this form pattern may contain either solidi or reverse solidi literally, without having to escape them.

<h2>Options</h2>

The behaviour of a Regexp can be modified by following the literal with one or more of the following option specifiers:

<ul>
<li>e Associate the pattern with the EUC-JP encoding</li>
<li>i Ignore case</li>
<li>m Let . match newline characters.</li>
<li>n Associate the pattern with the ASCII-8BIT encoding.</li>
<li>o Only interpolate #{…} constructs the first time this literal is parsed.</li>
<li>s Associate the pattern with the Windows-31J encoding.</li>
<li>u Associate the pattern with the UTF-8 encoding.</li>
<li>x Enable free-spacing mode.</li>
</ul>

Options i, m, and x, may also be applied to a specific group rather than the pattern as a whole, with the syntax described in Grouping.

From ruby.runpaint.org.
